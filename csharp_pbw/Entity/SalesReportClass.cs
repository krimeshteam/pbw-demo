﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csharp_pbw.Entity
{
    public class SalesReportClass
    {
        public string OrderNo { get; set; }
        public string SalesNo { get; set; }
        public int SalesId { get; set; }
        public DateTime SalesDate { get; set; }
        public string InvoiceType { get; set; }
        public string TransactionType { get; set; }
        public string PartyName { get; set; }
        public string State { get; set; }
        public int StateCode { get; set; }
        public string ShippingAddress { get; set; }
        public string PartyAddress { get; set; }
        public string PartyPhone { get; set; }
        public string PartyEmail { get; set; }
        public string PartyFax { get; set; }
        public string PartyGstIn { get; set; }
        public string Bank { get; set; }
        public string Payment { get; set; }
        public string ModeOfTransport { get; set; }
        public string LrNo { get; set; }
        public decimal Total { get; set; }
        public decimal GD_Perc { get; set; }
        public decimal CD_Perc { get; set; }
        public decimal CGST { get; set; }
        public decimal SGST { get; set; }
        public decimal IGST { get; set; }
        public int CGST_Perc { get; set; }
        public int SGST_Perc { get; set; }
        public int IGST_Perc { get; set; }
        public string DescriptionOfPackage { get; set; }
        public string ItemName { get; set; }
        public decimal Rate { get; set; }
        public int Qty { get; set; }


//        select SalesMaster.*, PartyMaster.PartyName, PartyMaster.Address as PartyAddress, PartyMaster.Phone as PartyPhone, PartyMaster.PartyName as PartyEmail, PartyMaster.PartyName as PartyFax, PartyMaster.PartyName as PartyGstIn, ItemMaster.ItemName
//from SalesMaster inner join SalesItemMaster on SalesMaster.SalesId = SalesItemMaster.SalesId
//inner join PartyMaster on SalesMaster.PartyId = PartyMaster.PartyId
//inner join ItemMaster on SalesItemMaster.ItemId = ItemMaster.ItemId
    }
}
