﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csharp_pbw.Entity
{
    public class ItemMaster
    {
        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemUnit { get; set; }
        public string HSNCode { get; set; }
        public char Rate { get; set; }
        public char Active { get; set; }
        
        public DateTime? Created_Date { get; set; }
        public DateTime? Updated_Date { get; set; }
        public DateTime? Deleted_Date { get; set; }
        public int? Created_By { get; set; }
        public int? Updated_By { get; set; }
        public int? Deleted_By { get; set; }
    }
}
