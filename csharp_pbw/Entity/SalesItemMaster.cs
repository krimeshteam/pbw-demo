﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csharp_pbw.Entity
{
    public class SalesItemMaster
    {
        public int SalesItemId { get; set; }
        public int SalesId { get; set; }
        public int ItemId { get; set; }
        public decimal Rate { get; set; }
        public int Qty { get; set; }
        public decimal Amount { get; set; }
        public int IsDelete { get; set; }
        
    }
}
