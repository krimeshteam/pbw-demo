﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp_pbw
{
    public partial class InvoiceList : Form
    {
        public InvoiceList()
        {
            InitializeComponent();
            bind();
        }
        public void bind()
        {

            string query = "select distinct SalesNo as InvoiceNo,PartyMaster.PartyName as Name,SalesDate as Date,InvoiceType,GrandTotal from SalesMaster inner join PartyMaster on SalesMaster.PartyId = PartyMaster.PartyId where SalesMaster.IsDelete=0";
            DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);

            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    datainvoicelist.DataSource = ds.Tables[0];
                }
                
            }
            

        }
        private void datainvoicelist_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var row = datainvoicelist.Rows[e.RowIndex].DataBoundItem as DataRowView;
                if (row != null)
                {
                    Common.InvoiceNo = row["InvoiceNo"].ToString();
                    this.Hide();
                }

            }
        }
    }
}
