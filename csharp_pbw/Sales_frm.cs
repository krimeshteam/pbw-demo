﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using csharp_pbw.Entity;
using csharp_pbw.Helper;

namespace csharp_pbw
{
    public partial class Sales_frm : Form
    {
        public DataSet ItemTempData = null;
        public List<ItemMaster> objlistitem = new List<ItemMaster>();
        public DataTable table = new DataTable();
        public int selectedid = 0;
        public int Selected_SalesId = 0;
        public decimal taxpar = 0;
        public string mode = "insert";

        public Sales_frm()
        {
            InitializeComponent();

            #region Table Columns

            table.Columns.Add("SalesItemId", typeof(int));
            table.Columns.Add("ItemId", typeof(int));
            table.Columns.Add("ItemName", typeof(string));
            table.Columns.Add("Qty", typeof(int));
            table.Columns.Add("Rate", typeof(decimal));
            table.Columns.Add("Amount", typeof(decimal));

            #endregion

            cmdinvoicetype.SelectedValue = "TAX";
            cmdtransaction.SelectedIndex = 0;

            PartyData();
            ItemData();
            StateData();
            ClearFunction("Cancel");
        }
        public void PrintInvoice(string id)
        {
            Common.ReportType = "PrintInvoice";
            Common.InvoiceNo = id;
            ReportDisplay obj = new ReportDisplay();
            obj.WindowState = FormWindowState.Maximized;
            obj.Text = "Invoice";
            obj.Show();
        }

        public void ClearFunction(string name)
        {


            if (name == "Edit")
            {
                btnadd.Enabled = false;
                btndelete.Enabled = true;
                btnedit.Enabled = true;
                btnreset.Enabled = true;


            }
            else if (name == "Cancel")
            {

                btnadd.Enabled = true;
                btndelete.Enabled = false;
                btnedit.Enabled = false;
                btnreset.Enabled = true;

                txtorderno.Enabled = true;
                txtorderno.Text = "";
                txtqty.Text = null;
                txtrate.Text = "0";
                txtamount.Text = "0";

                txttransport.Text = null;
                txtlrno.Text = null;
                txtbank.Text = null;
                txtpayment.Text = null;



                cmdinvoicetype.SelectedIndex = 0;
                cmdtransaction.SelectedIndex = 0;
                dropplaceofsupply.SelectedValue = "13";

                txtdate.Value = DateTime.Now;

                if (cmdpartyname.Items.Count > 0)
                {
                    cmdpartyname.SelectedIndex = 0;
                    cmdpartyname.Text = null;
                }
                if (cmditemname.Items.Count > 0)
                {
                    cmditemname.SelectedIndex = 0;
                    cmditemname.Text = null;

                }

                txttotal.Text = null;
                txtgendis_perc.Text = null;
                txtgeneraldiscount.Text = null;
                txtcashdis_perc.Text = null;
                txtcashdiscount.Text = null;

                txtcgst.Text = null;
                txtsgst.Text = null;
                txtigst.Text = null;

                txtgrandtot.Text = null;

                // dataitem.Rows.Clear();


                cmdinvoicetype.Focus();



                table.Rows.Clear();
                dataitem.DataSource = table;

            }
            else if (name == "IeamClear")
            {
                txtqty.Text = null;
                txtrate.Text = "0";
                txtamount.Text = null;
                cmditemname.Focus();

                btnadd.Enabled = true;
                btndelete.Enabled = false;
                btnedit.Enabled = false;
                btnreset.Enabled = true;
            }

        }

        private bool IsValidate()
        {
            bool Error = false;
            if (cmdpartyname.Items.Count == 0)
            {
                errorpartyname.SetError(cmdpartyname, "Select PartyName");
                Error = true;
            }
            return Error;
        }
        #region DataBind Dropdown
        public void StateData()
        {
            string query = "SELECT StateId,StateCode, State FROM StateMaster";
            DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
            if (ds.Tables.Count > 0)
            {
                // dataroute.DataSource = ds.Tables[0];

                dropplaceofsupply.DataSource = new BindingSource(ds.Tables[0], null);
                dropplaceofsupply.DisplayMember = "State";
                dropplaceofsupply.ValueMember = "StateId";
            }
            if (dropplaceofsupply.Items.Count > 0)
            {
                dropplaceofsupply.Text = null;
            }
        }
        public void PartyData()
        {
            string query = "SELECT PartyId, PartyName FROM PartyMaster";
            DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
            if (ds.Tables.Count > 0)
            {
                // dataroute.DataSource = ds.Tables[0];

                cmdpartyname.DataSource = new BindingSource(ds.Tables[0], null);
                cmdpartyname.DisplayMember = "PartyName";
                cmdpartyname.ValueMember = "PartyId";
            }
            if (cmdpartyname.Items.Count > 0)
            {
                cmdpartyname.Text = null;
            }
        }
        public void ItemData()
        {
            string query = "SELECT ItemId,ItemCode, ItemName,ItemUnit, HSNCode FROM ItemMaster";
            DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
            if (ds.Tables.Count > 0)
            {
                ItemTempData = ds;
                cmditemname.DataSource = new BindingSource(ds.Tables[0], null);
                cmditemname.DisplayMember = "ItemName";
                cmditemname.ValueMember = "ItemId";
            }
            if (cmditemname.Items.Count > 0)
            {
                cmditemname.Text = null;
            }
        }
        #endregion DataBind Dropdown

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmdtransaction_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            decimal qty = 0;
            decimal rate = 0;
            if (!string.IsNullOrEmpty(txtqty.Text))
            {
                qty = Convert.ToDecimal(txtqty.Text);
            }
            if (!string.IsNullOrEmpty(txtrate.Text))
            {
                rate = Convert.ToDecimal(txtrate.Text);
            }
            if (qty != 0 && rate != 0)
            {
                decimal tamount = qty * rate;
                txtamount.Text = tamount.ToString();
            }

        }

        private void txtrate_TextChanged(object sender, EventArgs e)
        {
            decimal qty = 0;
            decimal rate = 0;
            if (!string.IsNullOrEmpty(txtqty.Text))
            {
                qty = Convert.ToDecimal(txtqty.Text);
            }
            if (!string.IsNullOrEmpty(txtrate.Text))
            {
                rate = Convert.ToDecimal(txtrate.Text);
            }
            if (qty != 0 && rate != 0)
            {
                decimal tamount = qty * rate;
                txtamount.Text = tamount.ToString();
            }
        }
        #region Footer Calculation
        private void txttotal_TextChanged(object sender, EventArgs e)
        {
            decimal total = 0;
            decimal gd = 0;
            decimal cd = 0;
            if (!string.IsNullOrEmpty(txttotal.Text))
            {
                total = Convert.ToDecimal(txttotal.Text);
            }
            if (!string.IsNullOrEmpty(txtgendis_perc.Text))
            {
                gd = Convert.ToDecimal(txtgendis_perc.Text);
            }
            if (!string.IsNullOrEmpty(txtcashdis_perc.Text))
            {
                cd = Convert.ToDecimal(txtcashdis_perc.Text);
            }
            if (total > 0)
            {
                decimal gst_tot_val = total;
                if (gd > 0)
                {
                    decimal discvalue_gd = (total * gd / 100);
                    decimal gd_tot_amt = total - discvalue_gd;
                    txtgeneraldiscount.Text = discvalue_gd.ToString("#.##");
                    gst_tot_val = gd_tot_amt;
                }

                if (cd > 0)
                {
                    decimal discvalue_cd = (gst_tot_val * cd / 100);
                    decimal cd_tot_amt = gst_tot_val - discvalue_cd;
                    txtcashdiscount.Text = discvalue_cd.ToString("#.##");
                    gst_tot_val = cd_tot_amt;
                }

                int gst_val = (int)GST.TwentyEight;
                int gst = gst_val / 2;

                lbl_cgst_perc.Text = gst.ToString() + " %";
                lbl_sgst_perc.Text = gst.ToString() + " %";
                lbl_igst_perc.Text = gst_val.ToString() + " %";


                decimal gst_tot = (gst_tot_val * gst_val / 100);
                gst_tot_val = gst_tot_val + gst_tot;
                decimal gst_dev_val = (gst_tot / 2);

                txtcgst.Text = gst_dev_val.ToString("#.##");
                txtsgst.Text = gst_dev_val.ToString("#.##");
                txtigst.Text = gst_tot.ToString("#.##");

                txtgrandtot.Text = gst_tot_val.ToString("#.##");

            }

        }

        private void txtgendis_perc_TextChanged(object sender, EventArgs e)
        {
            decimal total = 0;
            decimal gd = 0;
            decimal cd = 0;
            if (!string.IsNullOrEmpty(txttotal.Text))
            {
                total = Convert.ToDecimal(txttotal.Text);
            }
            if (!string.IsNullOrEmpty(txtgendis_perc.Text))
            {
                gd = Convert.ToDecimal(txtgendis_perc.Text);
            }
            if (!string.IsNullOrEmpty(txtcashdis_perc.Text))
            {
                cd = Convert.ToDecimal(txtcashdis_perc.Text);
            }
            if (total > 0)
            {
                decimal gst_tot_val = total;
                if (gd > 0)
                {
                    decimal discvalue_gd = (total * gd / 100);
                    decimal gd_tot_amt = total - discvalue_gd;
                    txtgeneraldiscount.Text = discvalue_gd.ToString("#.##");
                    gst_tot_val = gd_tot_amt;
                }

                if (cd > 0)
                {
                    decimal discvalue_cd = (gst_tot_val * cd / 100);
                    decimal cd_tot_amt = gst_tot_val - discvalue_cd;
                    txtcashdiscount.Text = discvalue_cd.ToString("#.##");
                    gst_tot_val = cd_tot_amt;
                }

                int gst_val = (int)GST.TwentyEight;
                int gst = gst_val / 2;

                lbl_cgst_perc.Text = gst.ToString() + " %";
                lbl_sgst_perc.Text = gst.ToString() + " %";
                lbl_igst_perc.Text = gst_val.ToString() + " %";


                decimal gst_tot = (gst_tot_val * gst_val / 100);
                gst_tot_val = gst_tot_val + gst_tot;
                decimal gst_dev_val = (gst_tot / 2);

                txtcgst.Text = gst_dev_val.ToString("#.##");
                txtsgst.Text = gst_dev_val.ToString("#.##");
                txtigst.Text = gst_tot.ToString("#.##");

                txtgrandtot.Text = gst_tot_val.ToString("#.##");

            }
        }

        private void txtgeneraldiscount_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcashdis_perc_TextChanged(object sender, EventArgs e)
        {
            decimal total = 0;
            decimal gd = 0;
            decimal cd = 0;
            if (!string.IsNullOrEmpty(txttotal.Text))
            {
                total = Convert.ToDecimal(txttotal.Text);
            }
            if (!string.IsNullOrEmpty(txtgendis_perc.Text))
            {
                gd = Convert.ToDecimal(txtgendis_perc.Text);
            }
            if (!string.IsNullOrEmpty(txtcashdis_perc.Text))
            {
                cd = Convert.ToDecimal(txtcashdis_perc.Text);
            }
            if (total > 0)
            {
                decimal gst_tot_val = total;
                if (gd > 0)
                {
                    decimal discvalue_gd = (total * gd / 100);
                    decimal gd_tot_amt = total - discvalue_gd;
                    txtgeneraldiscount.Text = discvalue_gd.ToString("#.##");
                    gst_tot_val = gd_tot_amt;
                }

                if (cd > 0)
                {
                    decimal discvalue_cd = (gst_tot_val * cd / 100);
                    decimal cd_tot_amt = gst_tot_val - discvalue_cd;
                    txtcashdiscount.Text = discvalue_cd.ToString("#.##");
                    gst_tot_val = cd_tot_amt;
                }

                int gst_val = (int)GST.TwentyEight;
                int gst = gst_val / 2;

                lbl_cgst_perc.Text = gst.ToString() + " %";
                lbl_sgst_perc.Text = gst.ToString() + " %";
                lbl_igst_perc.Text = gst_val.ToString() + " %";


                decimal gst_tot = (gst_tot_val * gst_val / 100);
                gst_tot_val = gst_tot_val + gst_tot;
                decimal gst_dev_val = (gst_tot / 2);

                txtcgst.Text = gst_dev_val.ToString("#.##");
                txtsgst.Text = gst_dev_val.ToString("#.##");
                txtigst.Text = gst_tot.ToString("#.##");

                txtgrandtot.Text = gst_tot_val.ToString("#.##");

            }
        }

        private void txtcashdiscount_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtgeneraldiscount_Leave(object sender, EventArgs e)
        {
            //Discount %= (Discount Amount / MRP) * 100
            decimal total = 0;
            decimal gd = 0;
            decimal gd_amt = 0;
            decimal cd = 0;
            if (!string.IsNullOrEmpty(txttotal.Text))
            {
                total = Convert.ToDecimal(txttotal.Text);
            }
            if (!string.IsNullOrEmpty(txtgendis_perc.Text))
            {
                gd = Convert.ToDecimal(txtgendis_perc.Text);
            }
            if (!string.IsNullOrEmpty(txtgeneraldiscount.Text))
            {
                gd_amt = Convert.ToDecimal(txtgeneraldiscount.Text);
            }
            if (!string.IsNullOrEmpty(txtcashdis_perc.Text))
            {
                cd = Convert.ToDecimal(txtcashdis_perc.Text);
            }
            if (total > 0)
            {
                decimal gst_tot_val = total;
                if (gd_amt > 0)
                {
                    decimal discvalue_perc = ((gd_amt / total) * 100);
                    txtgendis_perc.Text = discvalue_perc.ToString("#.##");
                    decimal gd_tot_amt = total - gd_amt;
                    gst_tot_val = gd_tot_amt;
                }

                //if (gd > 0)
                //{
                //    decimal discvalue_gd = ((total * gd) / 100);
                //    decimal gd_tot_amt = total - discvalue_gd;
                //    txtgeneraldiscount.Text = discvalue_gd.ToString("#.##");
                //}

                if (cd > 0)
                {
                    decimal discvalue_cd = (gst_tot_val * cd / 100);
                    decimal cd_tot_amt = gst_tot_val - discvalue_cd;
                    txtcashdiscount.Text = discvalue_cd.ToString("#.##");
                    gst_tot_val = cd_tot_amt;
                }

                int gst_val = (int)GST.TwentyEight;
                int gst = gst_val / 2;

                lbl_cgst_perc.Text = gst.ToString() + " %";
                lbl_sgst_perc.Text = gst.ToString() + " %";
                lbl_igst_perc.Text = gst_val.ToString() + " %";


                decimal gst_tot = (gst_tot_val * gst_val / 100);
                gst_tot_val = gst_tot_val + gst_tot;
                decimal gst_dev_val = (gst_tot / 2);

                txtcgst.Text = gst_dev_val.ToString("#.##");
                txtsgst.Text = gst_dev_val.ToString("#.##");
                txtigst.Text = gst_tot.ToString("#.##");

                txtgrandtot.Text = gst_tot_val.ToString("#.##");
            }

        }
        private void txtcashdiscount_Leave(object sender, EventArgs e)
        {
            //Discount %= (Discount Amount / MRP) * 100
            decimal total = 0;
            decimal gd = 0;
            decimal cd_amt = 0;
            decimal cd = 0;
            if (!string.IsNullOrEmpty(txttotal.Text))
            {
                total = Convert.ToDecimal(txttotal.Text);
            }
            if (!string.IsNullOrEmpty(txtgendis_perc.Text))
            {
                gd = Convert.ToDecimal(txtgendis_perc.Text);
            }
            if (!string.IsNullOrEmpty(txtcashdiscount.Text))
            {
                cd_amt = Convert.ToDecimal(txtcashdiscount.Text);
            }
            if (!string.IsNullOrEmpty(txtcashdis_perc.Text))
            {
                cd = Convert.ToDecimal(txtcashdis_perc.Text);
            }
            if (total > 0)
            {
                decimal gst_tot_val = total;


                if (gd > 0)
                {
                    decimal discvalue_gd = (total * gd / 100);
                    decimal gd_tot_amt = total - discvalue_gd;
                    txtgeneraldiscount.Text = discvalue_gd.ToString("#.##");
                    gst_tot_val = gd_tot_amt;
                }

                if (cd_amt > 0)
                {
                    decimal discvalue_perc = ((cd_amt / total) * 100);
                    txtcashdis_perc.Text = discvalue_perc.ToString("#.##");
                    decimal cd_tot_amt = gst_tot_val - cd_amt;
                    gst_tot_val = cd_tot_amt;
                }

                //if (cd > 0)
                //{
                //    decimal discvalue_cd = (gst_tot_val * cd / 100);

                //    txtcashdiscount.Text = discvalue_cd.ToString("#.##");
                //    gst_tot_val = cd_tot_amt;
                //}

                int gst_val = (int)GST.TwentyEight;
                int gst = gst_val / 2;

                lbl_cgst_perc.Text = gst.ToString() + " %";
                lbl_sgst_perc.Text = gst.ToString() + " %";
                lbl_igst_perc.Text = gst_val.ToString() + " %";


                decimal gst_tot = (gst_tot_val * gst_val / 100);
                gst_tot_val = gst_tot_val + gst_tot;
                decimal gst_dev_val = (gst_tot / 2);

                txtcgst.Text = gst_dev_val.ToString("#.##");
                txtsgst.Text = gst_dev_val.ToString("#.##");
                txtigst.Text = gst_tot.ToString("#.##");

                txtgrandtot.Text = gst_tot_val.ToString("#.##");
            }
        }
        #endregion Footer Calculation
        #region KeyPress
        private void txtgendis_perc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Back) //The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }

        private void txtgeneraldiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Back || e.KeyChar == '.') //The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }

        private void txtcashdis_perc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Back) //The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }

        private void txtcashdiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Back || e.KeyChar == '.') //The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Back || e.KeyChar == '.') //The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }

        private void txtrate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Back || e.KeyChar == '.') //The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }

        private void txtamount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Back || e.KeyChar == '.') //The  character represents a backspace
            {
                e.Handled = false; //Do not reject the input
            }
            else
            {
                e.Handled = true; //Reject the input
            }
        }
        #endregion KeyPress

        #region Item
        public int ErrorAdd()
        {
            if (cmditemname.SelectedValue == null)
            {
                MessageBox.Show("Select Item Name");
                cmditemname.Focus();
                return 0;
            }
            else if (txtqty.Text == "" || txtqty.Text == null)
            {
                MessageBox.Show("Enter Qty");
                txtqty.Focus();
                return 0;
            }
            else if (txtrate.Text == "" || txtrate.Text == null)
            {
                MessageBox.Show("Enter Rate");
                txtrate.Focus();
                return 0;
            }
            else if (txtamount.Text == "" || txtamount.Text == null)
            {
                MessageBox.Show("Enter Amount");
                txtamount.Focus();
                return 0;
            }
            else
            {
                return 1;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            int res = ErrorAdd();
            if (res == 1)
            {
                int salesitemid = 0;
                int itemid = Convert.ToInt32(cmditemname.SelectedValue);
                string itemname = cmditemname.Text;
                int qty = Convert.ToInt32(txtqty.Text);
                decimal rate = Convert.ToDecimal(txtrate.Text);
                decimal amount = Convert.ToDecimal(txtamount.Text);
                table.Rows.Add(salesitemid, itemid, itemname, qty, rate, amount);
                ClearFunction("IeamClear");
                if (table.Rows.Count > 0)
                {

                    dataitem.DataSource = table;
                }

            }
        }
        private void btnedit_Click(object sender, EventArgs e)
        {
            var row = dataitem.Rows[selectedid].DataBoundItem as DataRowView;
            if (row != null)
            {

                int itemid = Convert.ToInt32(cmditemname.SelectedValue);
                string itemname = cmditemname.Text;
                int qty = Convert.ToInt32(txtqty.Text);
                decimal rate = Convert.ToDecimal(txtrate.Text);
                decimal amount = Convert.ToDecimal(txtamount.Text);

                row["ItemId"] = itemid;
                row["ItemName"] = itemname;
                row["Qty"] = qty;
                row["Rate"] = rate;
                row["Amount"] = amount;

            }
            row.EndEdit();

            table.AcceptChanges();
            ClearFunction("IeamClear");
        }
        private void btndelete_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure to delete Record?", "Confirmation", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                for (int i = 0; i < dataitem.Rows.Count; i++)
                {
                    var row = dataitem.Rows[i].DataBoundItem as DataRowView;
                    if (row != null)
                    {
                        // int no = Convert.ToInt32(row["No"].ToString());
                        if (i == selectedid)
                        {
                            row.Delete();
                            break;
                        }
                    }
                }
                ClearFunction("IeamClear");
                LoadSerial();
            }
        }

        private void btnreset_Click(object sender, EventArgs e)
        {
            ClearFunction("IeamClear");
        }
        public void LoadSerial()
        {
            int i = 1;
            foreach (DataGridViewRow row in dataitem.Rows)
            {
                row.Cells["No"].Value = i; i++;
            }
        }

        private void dataitem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var row = dataitem.Rows[e.RowIndex].DataBoundItem as DataRowView;
                if (row != null)
                {
                    selectedid = e.RowIndex;

                    int sid = Convert.ToInt32(row["SalesItemId"].ToString());
                    cmditemname.SelectedValue = Convert.ToInt32(row["ItemId"].ToString());

                    txtqty.Text = row["Qty"].ToString();
                    txtrate.Text = row["Rate"].ToString();
                    txtamount.Text = row["Amount"].ToString();
                    ClearFunction("Edit");

                }
            }
        }

        private void dataitem_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            LoadSerial();
            double result = dataitem.Rows.Cast<DataGridViewRow>().Sum(row => Convert.ToDouble(row.Cells["Amount"].Value));
            txttotal.Text = result.ToString();

        }


        #endregion Item

        #region Ok,Cancel,Exit
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void btnok_Click(object sender, EventArgs e)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            string invo = "";
            if (string.IsNullOrEmpty(txtinvoiceno.Text))
            {
                Random rnd = new Random();
                invo = "IN-" + rnd.Next(100000);
            }
            else
            {
                invo = txtinvoiceno.Text;
            }
            string d = txtdate.Value.ToString();
            DateTime date = Convert.ToDateTime(d);
          

            parameters.Add(new SqlParameter("@OrderNo", txtorderno.Text.Trim()));
            parameters.Add(new SqlParameter("@SalesNo", invo));
            parameters.Add(new SqlParameter("@SalesDate", date));
            parameters.Add(new SqlParameter("@InvoiceType", cmdinvoicetype.Text));
            parameters.Add(new SqlParameter("@TransactionType", cmdtransaction.Text));
            parameters.Add(new SqlParameter("@PartyId", cmdpartyname.SelectedValue));
            parameters.Add(new SqlParameter("@PlaceofSupply", dropplaceofsupply.SelectedValue));
            parameters.Add(new SqlParameter("@ShippingAddress", txtshippingaddress.Text));
            parameters.Add(new SqlParameter("@Bank", txtbank.Text.Trim()));
            parameters.Add(new SqlParameter("@Payment", txtpayment.Text.Trim()));
            parameters.Add(new SqlParameter("@ModeOfTransport", txttransport.Text.Trim()));
            parameters.Add(new SqlParameter("@LrNo", txtlrno.Text.Trim()));
            decimal total = 0;
            if (!string.IsNullOrEmpty(txttotal.Text.Trim()))
            {
                total = Convert.ToDecimal(txttotal.Text.Trim());
            }
            parameters.Add(new SqlParameter("@Total", total));
            decimal gendis_perc = 0;
            if (!string.IsNullOrEmpty(txtgendis_perc.Text.Trim()))
            {
                gendis_perc = Convert.ToDecimal(txtgendis_perc.Text.Trim());
            }
            parameters.Add(new SqlParameter("@GD_Perc", gendis_perc));
            decimal cashdis_perc = 0;
            if (!string.IsNullOrEmpty(txtcashdis_perc.Text.Trim()))
            {
                cashdis_perc = Convert.ToDecimal(txtcashdis_perc.Text.Trim());
            }
            parameters.Add(new SqlParameter("@CD_Perc", cashdis_perc));
            decimal CGST = 0;
            if (!string.IsNullOrEmpty(txtcgst.Text.Trim()))
            {
                CGST = Convert.ToDecimal(txtcgst.Text.Trim());
            }
            parameters.Add(new SqlParameter("@CGST", CGST));
            decimal SGST = 0;
            if (!string.IsNullOrEmpty(txtsgst.Text.Trim()))
            {
                SGST = Convert.ToDecimal(txtsgst.Text.Trim());
            }
            parameters.Add(new SqlParameter("@SGST", SGST));
            decimal IGST = 0;
            if (!string.IsNullOrEmpty(txtigst.Text.Trim()))
            {
                IGST = Convert.ToDecimal(txtigst.Text.Trim());
            }
            parameters.Add(new SqlParameter("@IGST", IGST));

            int CGST_Perc = 0;
            if (!string.IsNullOrEmpty(lbl_cgst_perc.Text.Trim()))
            {
                CGST_Perc = Convert.ToInt32(lbl_cgst_perc.Text.TrimEnd('%'));
            }
            parameters.Add(new SqlParameter("@CGST_Perc", CGST_Perc));
            int SGST_Perc = 0;
            if (!string.IsNullOrEmpty(lbl_sgst_perc.Text.Trim()))
            {
                SGST_Perc = Convert.ToInt32(lbl_sgst_perc.Text.TrimEnd('%'));
            }
            parameters.Add(new SqlParameter("@SGST_Perc", SGST_Perc));
            int IGST_Perc = 0;
            if (!string.IsNullOrEmpty(lbl_igst_perc.Text.Trim()))
            {
                IGST_Perc = Convert.ToInt32(lbl_igst_perc.Text.TrimEnd('%'));
            }
            parameters.Add(new SqlParameter("@IGST_Perc", IGST_Perc));

            parameters.Add(new SqlParameter("@DescriptionOfPackage", null));

            decimal grandtot = 0;
            if (!string.IsNullOrEmpty(txtgrandtot.Text.Trim()))
            {
                grandtot = Convert.ToDecimal(txtgrandtot.Text.Trim());
            }
            parameters.Add(new SqlParameter("@GrandTotal", grandtot));

            string query = null;
            int SalesId = 0;
            if (mode == "edit")
            {
                parameters.Add(new SqlParameter("@Updated_Date", DateTime.Now));
                parameters.Add(new SqlParameter("@Updated_By", 1));
                parameters.Add(new SqlParameter("@SalesId", Selected_SalesId));
                query = "Update SalesMaster SET SalesDate=@SalesDate,InvoiceType=@InvoiceType,TransactionType=@TransactionType,PartyId=@PartyId,PlaceofSupply=@PlaceofSupply,ShippingAddress=@ShippingAddress,Bank=@Bank,Payment=@Payment,ModeOfTransport=@ModeOfTransport,LrNo=@LrNo,Total=@Total,GD_Perc=@GD_Perc,CD_Perc=@CD_Perc,CGST=@CGST,SGST=@SGST,IGST=@IGST,CGST_Perc=@CGST_Perc,SGST_Perc=@SGST_Perc,IGST_Perc=@IGST_Perc,DescriptionOfPackage=@DescriptionOfPackage,Updated_Date=@Updated_Date,Updated_By=@Updated_By,GrandTotal=@GrandTotal where SalesId=@SalesId";
                SqlHelper.ExecuteNonQuery(ConnString.GetConnString(), CommandType.Text, query, parameters.ToArray());
                //Temp Delete All Sales Item Row
                query = string.Format("Update SalesItemMaster SET IsDelete={0} where SalesId={1}", 1, Selected_SalesId);
                SqlHelper.ExecuteNonQuery(ConnString.GetConnString(), CommandType.Text, query);


                SalesId = Selected_SalesId;
            }
            else
            {
                parameters.Add(new SqlParameter("@Created_Date", DateTime.Now));
                parameters.Add(new SqlParameter("@Created_by", 1));
                query = "INSERT INTO SalesMaster (OrderNo,SalesNo,SalesDate,InvoiceType,TransactionType,PartyId,PlaceofSupply,ShippingAddress,Bank,Payment,ModeOfTransport,LrNo,Total,GD_Perc,CD_Perc,CGST,SGST,IGST,CGST_Perc,SGST_Perc,IGST_Perc,DescriptionOfPackage,Created_Date,Created_by,GrandTotal,IsDelete) VALUES (@OrderNo,@SalesNo,@SalesDate,@InvoiceType,@TransactionType,@PartyId,@PlaceofSupply,@ShippingAddress,@Bank,@Payment,@ModeOfTransport,@LrNo,@Total,@GD_Perc,@CD_Perc,@CGST,@SGST,@IGST,@CGST_Perc,@SGST_Perc,@IGST_Perc,@DescriptionOfPackage,@Created_Date,@Created_by,@GrandTotal,0);SELECT SCOPE_IDENTITY();";
                SalesId = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnString.GetConnString(), CommandType.Text, query, parameters.ToArray()));

            }

            if (dataitem.Rows.Count > 0)
            {

                for (int i = 0; i < dataitem.Rows.Count; i++)
                {
                    SalesItemMaster objSalesItemMaster = new SalesItemMaster();

                    var row = dataitem.Rows[i].DataBoundItem as DataRowView;
                    if (row != null)
                    {
                        int SalesItemId = Convert.ToInt32(row["SalesItemId"].ToString());

                        int ItemId = Convert.ToInt32(row["ItemId"].ToString());
                        int Qty = Convert.ToInt32(row["Qty"].ToString());
                        decimal Rate = Convert.ToDecimal(row["Rate"].ToString());
                        decimal Amount = Convert.ToDecimal(row["Amount"].ToString());

                        objSalesItemMaster.ItemId = ItemId;
                        objSalesItemMaster.SalesId = SalesId;
                        objSalesItemMaster.Qty = Qty;
                        objSalesItemMaster.Rate = Rate;
                        objSalesItemMaster.Amount = Amount;
                        objSalesItemMaster.IsDelete = 0;

                        if (SalesItemId > 0)
                        {
                            objSalesItemMaster.SalesItemId = SalesItemId;
                            query = string.Format("Update SalesItemMaster SET ItemId={0},Qty={1},Rate={2},Amount={3},IsDelete={4} where SalesItemId={5}", objSalesItemMaster.ItemId, objSalesItemMaster.Qty, objSalesItemMaster.Rate, objSalesItemMaster.Amount, 0, SalesItemId);
                            SqlHelper.ExecuteNonQuery(ConnString.GetConnString(), CommandType.Text, query);

                        }
                        else
                        {
                            query = string.Format("Insert Into SalesItemMaster(ItemId,SalesId,Qty,Rate,Amount,IsDelete) values({0},{1},{2},{3},{4},{5})", objSalesItemMaster.ItemId, objSalesItemMaster.SalesId, objSalesItemMaster.Qty, objSalesItemMaster.Rate, objSalesItemMaster.Amount, 0);
                            SqlHelper.ExecuteNonQuery(ConnString.GetConnString(), CommandType.Text, query);
                        }

                    }
                }
                if (mode == "edit")
                {
                    ClearFunction("Cancel");
                    MessageBox.Show("invoice successfully updated");
                    mode = "insert";
                    txtinvoiceno.Text = null;
                }
                else
                {
                    ClearFunction("Cancel");
                    MessageBox.Show("invoice successfully created");
                    mode = "insert";
                    txtinvoiceno.Text = null;
                }
                PrintInvoice(Selected_SalesId.ToString());


            }

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            cmdinvoicetype.SelectedIndex = 0;
            cmdtransaction.SelectedIndex = 0;

            ClearFunction("Cancel");
            mode = "insert";
            txtinvoiceno.Text = null;
        }

        #endregion Ok,Cancel,Exit

        private void txtinvoiceno_TextChanged(object sender, EventArgs e)
        {
            if (txtinvoiceno.Text.Count() > 0)
            {
                btndeleteinvoice.Enabled = true;
            }
            else
            {
                btndeleteinvoice.Enabled = true;
            }
        }

        private void btninvoicelist_Click(object sender, EventArgs e)
        {
            InvoiceList testDialog = new InvoiceList();

            // Show testDialog as a modal dialog and determine if DialogResult = OK. 
            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {

            }
            else
            {
                //close popup
                string invoi = Common.InvoiceNo;
                if (invoi != null && invoi != "")
                {
                    InvoiceViewEdit(invoi);
                }
                Common.InvoiceNo = "";
            }
            testDialog.Dispose();
        }
        public void InvoiceViewEdit(string invoice)
        {
            table.Rows.Clear();
            dataitem.DataSource = table;

            txtinvoiceno.Text = invoice.ToString();

            string query = "SELECT * FROM SalesMaster where SalesNo='" + invoice + "'";
            DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);

            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    mode = "edit";
                    cmdtransaction.SelectedValue = ds.Tables[0].Rows[0]["TransactionType"].ToString();
                    cmdinvoicetype.SelectedValue = ds.Tables[0].Rows[0]["InvoiceType"].ToString();
                    txtinvoiceno.Text = ds.Tables[0].Rows[0]["SalesNo"].ToString();
                    txtorderno.Text = ds.Tables[0].Rows[0]["OrderNo"].ToString();
                    txtdate.Value = Convert.ToDateTime(ds.Tables[0].Rows[0]["SalesDate"].ToString());
                    cmdpartyname.SelectedValue = ds.Tables[0].Rows[0]["PartyId"].ToString();
                    dropplaceofsupply.SelectedValue = ds.Tables[0].Rows[0]["PlaceofSupply"].ToString();
                    txtshippingaddress.Text = ds.Tables[0].Rows[0]["ShippingAddress"].ToString();

                    txtbank.Text = ds.Tables[0].Rows[0]["Bank"].ToString();
                    txtpayment.Text = ds.Tables[0].Rows[0]["Payment"].ToString();
                    txttransport.Text = ds.Tables[0].Rows[0]["ModeOfTransport"].ToString();
                    txtlrno.Text = ds.Tables[0].Rows[0]["LrNo"].ToString();

                    txttotal.Text = ds.Tables[0].Rows[0]["Total"].ToString();
                    txtgendis_perc.Text = ds.Tables[0].Rows[0]["GD_Perc"].ToString();
                    txtcashdis_perc.Text = ds.Tables[0].Rows[0]["CD_Perc"].ToString();
                    dataitem.AutoGenerateColumns = false;

                    string SalesId = ds.Tables[0].Rows[0]["SalesId"].ToString();
                    Selected_SalesId = Convert.ToInt32(SalesId);
                    string query_item = "Select SalesItemMaster.*,ItemMaster.ItemName from SalesItemMaster inner join ItemMaster on SalesItemMaster.ItemId = ItemMaster.ItemId where SalesItemMaster.SalesId=" + SalesId;
                    DataSet ds_SalesItem = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query_item);

                    // dataitem.DataSource = ds.Tables[0];
                    foreach (DataRow dr in ds_SalesItem.Tables[0].Rows)
                    {
                        int SalesItemId = Convert.ToInt32(dr["SalesItemId"].ToString());
                        int itemid = Convert.ToInt32(dr["ItemId"].ToString());

                        string itemname = dr["ItemName"].ToString();
                        int qty = Convert.ToInt32(dr["Qty"].ToString());
                        decimal rate = Convert.ToDecimal(dr["Rate"].ToString());
                        decimal amount = Convert.ToDecimal(dr["Amount"].ToString());


                        table.Rows.Add(SalesItemId, itemid, itemname, qty, rate, amount);
                    }
                    dataitem.DataSource = table;

                   
                    txtorderno.Enabled = false;
                }
            }
        }

        private void btndeleteinvoice_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure to delete Record?", "Confirmation", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                string InvoiceNo = txtinvoiceno.Text;

                string query = string.Format("Update SalesMaster IsDelete={0} where SalesNo={1}", 1, InvoiceNo);
                int returnStatus = (int)SqlHelper.ExecuteNonQuery(ConnString.GetConnString(), CommandType.Text, query);

                if (returnStatus > 0)
                {
                    ClearFunction("Cancel");
                    MessageBox.Show("Record Delete");
                    mode = "insert";

                }
                txtinvoiceno.Text = null;
                cmditemname.SelectedIndex = 0;
            }
        }

        private void txtorderno_TextChanged(object sender, EventArgs e)
        {
            if(mode == "insert")
            {
                table.Rows.Clear();
                dataitem.DataSource = table;

                string orderno = txtorderno.Text;

                string query = "SELECT * FROM OrderMaster where OrderNO='" + orderno + "'";
                DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);

                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        cmdpartyname.SelectedValue = ds.Tables[0].Rows[0]["PartyId"].ToString();
                        string OrderId = ds.Tables[0].Rows[0]["OrderId"].ToString();
                        string query_item = "Select OrderItemMaster.*,ItemMaster.ItemName from OrderItemMaster inner join ItemMaster on OrderItemMaster.ItemId = ItemMaster.ItemId where OrderItemMaster.OrderId=" + OrderId;
                        DataSet ds_SalesItem = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query_item);

                        // dataitem.DataSource = ds.Tables[0];
                        foreach (DataRow dr in ds_SalesItem.Tables[0].Rows)
                        {
                            int SalesItemId = 0;
                            int itemid = Convert.ToInt32(dr["ItemId"].ToString());

                            string itemname = dr["ItemName"].ToString();
                            int qty = Convert.ToInt32(dr["Qty"].ToString());
                            decimal rate = Convert.ToDecimal(dr["Rate"].ToString());
                            decimal amount = Convert.ToDecimal(qty) * Convert.ToDecimal(rate);


                            table.Rows.Add(SalesItemId, itemid, itemname, qty, rate, amount);
                        }
                        dataitem.DataSource = table;

                    }
                }
            }
            
        }

        private void dropplaceofsupply_SelectedIndexChanged(object sender, EventArgs e)
        {
            string state = dropplaceofsupply.Text.ToString();
            if (state == "Gujarat")
            {
                lbl_cgst_perc.Visible = true;
                lbl_sgst_perc.Visible = true;
                lbl_igst_perc.Visible = false;

                label5.Visible = true;
                label6.Visible = true;
                label7.Visible = false;

                txtcgst.Visible = true;
                txtsgst.Visible = true;
                txtigst.Visible = false;
            }
            else
            {
                lbl_cgst_perc.Visible = false;
                lbl_sgst_perc.Visible = false;
                lbl_igst_perc.Visible = true;

                label5.Visible = false;
                label6.Visible = false;
                label7.Visible = true;

                txtcgst.Visible = false;
                txtsgst.Visible = false;
                txtigst.Visible = true;
            }
        }


    }
}
