﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace csharp_pbw
{
    public class ConnString
    {
        
        public static SqlConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection(GetConnString());
            connection.Open();

            return connection;
        }

       
        public static string GetConnString()
        {

            string res = null;
            //res = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database\PatelDatabase.mdf;Initial Catalog=PatelDatabase;Integrated Security=True;Connect Timeout=30;User Instance=True";
            res = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename="+ System.Windows.Forms.Application.StartupPath + @"\Database\PatelDatabase.mdf;Integrated Security=True";
            return res;
        }


    }
}
