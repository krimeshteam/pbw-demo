﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp_pbw
{
    public partial class ReportForm : Form
    {
        public ReportForm()
        {
            InitializeComponent();
            lblinvoiceno.Visible = false;
            txtinvoiceno.Visible = false;
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            ReportDisplay testDialog = new ReportDisplay();
            string messageType = string.Empty;
            if (radio_item.Checked)
            {
                Common.ReportType = "Item";
            }
            else if (radio_party.Checked)
            {
                Common.ReportType = "Party";
            }
            else if (radio_order.Checked)
            {
                Common.ReportType = "Order";
            }
            else if (radio_invoice.Checked)
            {
                Common.ReportType = "InvoiceList";
            }
            else if (radio_PrintInvoice.Checked)
            {
                Common.ReportType = "PrintInvoice";
                if (!string.IsNullOrEmpty(txtinvoiceno.Text))
                {
                    string query = "SELECT SalesId FROM SalesMaster where IsDelete=0 and SalesNo='" + txtinvoiceno.Text + "'";
                    DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string salesid = ds.Tables[0].Rows[0][0].ToString();
                            Common.InvoiceNo = salesid;
                        }
                    }

                }
                else
                {
                    messageType = "Empty";
                }
            }

            if (Common.ReportType == "PrintInvoice")
            {
                if (messageType == "Empty")
                {
                    MessageBox.Show("Please Enter Invoice No.");
                }
                else if (string.IsNullOrEmpty(messageType))
                {
                    MessageBox.Show("Please Enter Valid Invoice No.");
                }
                else
                {
                    if (testDialog.ShowDialog(this) == DialogResult.OK)
                    {

                    }
                }

            }
            else
            {
                if (testDialog.ShowDialog(this) == DialogResult.OK)
                {

                }
            }

        }

        private void radio_PrintInvoice_CheckedChanged(object sender, EventArgs e)
        {
            if (radio_PrintInvoice.Checked)
            {
                lblinvoiceno.Visible = true;
                txtinvoiceno.Visible = true;
            }
            else
            {
                lblinvoiceno.Visible = false;
                txtinvoiceno.Visible = false;
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
