﻿namespace csharp_pbw
{
    partial class ReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtinvoiceno = new System.Windows.Forms.TextBox();
            this.lblinvoiceno = new System.Windows.Forms.Label();
            this.radio_PrintInvoice = new System.Windows.Forms.RadioButton();
            this.radio_invoice = new System.Windows.Forms.RadioButton();
            this.btnprint = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.radio_order = new System.Windows.Forms.RadioButton();
            this.radio_party = new System.Windows.Forms.RadioButton();
            this.radio_item = new System.Windows.Forms.RadioButton();
            this.btnexit = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.Controls.Add(this.btnexit);
            this.panel1.Controls.Add(this.txtinvoiceno);
            this.panel1.Controls.Add(this.lblinvoiceno);
            this.panel1.Controls.Add(this.radio_PrintInvoice);
            this.panel1.Controls.Add(this.radio_invoice);
            this.panel1.Controls.Add(this.btnprint);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.radio_order);
            this.panel1.Controls.Add(this.radio_party);
            this.panel1.Controls.Add(this.radio_item);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(708, 305);
            this.panel1.TabIndex = 0;
            // 
            // txtinvoiceno
            // 
            this.txtinvoiceno.Location = new System.Drawing.Point(151, 107);
            this.txtinvoiceno.Name = "txtinvoiceno";
            this.txtinvoiceno.Size = new System.Drawing.Size(184, 20);
            this.txtinvoiceno.TabIndex = 9;
            // 
            // lblinvoiceno
            // 
            this.lblinvoiceno.AutoSize = true;
            this.lblinvoiceno.Location = new System.Drawing.Point(55, 114);
            this.lblinvoiceno.Name = "lblinvoiceno";
            this.lblinvoiceno.Size = new System.Drawing.Size(90, 13);
            this.lblinvoiceno.TabIndex = 8;
            this.lblinvoiceno.Text = "Enter Invoice No.";
            // 
            // radio_PrintInvoice
            // 
            this.radio_PrintInvoice.AutoSize = true;
            this.radio_PrintInvoice.Location = new System.Drawing.Point(303, 68);
            this.radio_PrintInvoice.Name = "radio_PrintInvoice";
            this.radio_PrintInvoice.Size = new System.Drawing.Size(60, 17);
            this.radio_PrintInvoice.TabIndex = 7;
            this.radio_PrintInvoice.TabStop = true;
            this.radio_PrintInvoice.Text = "Invoice";
            this.radio_PrintInvoice.UseVisualStyleBackColor = true;
            this.radio_PrintInvoice.CheckedChanged += new System.EventHandler(this.radio_PrintInvoice_CheckedChanged);
            // 
            // radio_invoice
            // 
            this.radio_invoice.AutoSize = true;
            this.radio_invoice.Location = new System.Drawing.Point(217, 68);
            this.radio_invoice.Name = "radio_invoice";
            this.radio_invoice.Size = new System.Drawing.Size(79, 17);
            this.radio_invoice.TabIndex = 6;
            this.radio_invoice.Text = "Invoice List";
            this.radio_invoice.UseVisualStyleBackColor = true;
            // 
            // btnprint
            // 
            this.btnprint.Location = new System.Drawing.Point(50, 192);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(75, 23);
            this.btnprint.TabIndex = 5;
            this.btnprint.Text = "Print";
            this.btnprint.UseVisualStyleBackColor = true;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select Report";
            // 
            // radio_order
            // 
            this.radio_order.AutoSize = true;
            this.radio_order.Location = new System.Drawing.Point(160, 68);
            this.radio_order.Name = "radio_order";
            this.radio_order.Size = new System.Drawing.Size(51, 17);
            this.radio_order.TabIndex = 3;
            this.radio_order.Text = "Order";
            this.radio_order.UseVisualStyleBackColor = true;
            // 
            // radio_party
            // 
            this.radio_party.AutoSize = true;
            this.radio_party.Location = new System.Drawing.Point(103, 68);
            this.radio_party.Name = "radio_party";
            this.radio_party.Size = new System.Drawing.Size(49, 17);
            this.radio_party.TabIndex = 2;
            this.radio_party.Text = "Party";
            this.radio_party.UseVisualStyleBackColor = true;
            // 
            // radio_item
            // 
            this.radio_item.AutoSize = true;
            this.radio_item.Checked = true;
            this.radio_item.Location = new System.Drawing.Point(50, 68);
            this.radio_item.Name = "radio_item";
            this.radio_item.Size = new System.Drawing.Size(45, 17);
            this.radio_item.TabIndex = 1;
            this.radio_item.TabStop = true;
            this.radio_item.Text = "Item";
            this.radio_item.UseVisualStyleBackColor = true;
            // 
            // btnexit
            // 
            this.btnexit.Location = new System.Drawing.Point(132, 192);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(75, 23);
            this.btnexit.TabIndex = 10;
            this.btnexit.Text = "Exit";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(733, 330);
            this.Controls.Add(this.panel1);
            this.Name = "ReportForm";
            this.Text = "Report";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radio_order;
        private System.Windows.Forms.RadioButton radio_party;
        private System.Windows.Forms.RadioButton radio_item;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnprint;
        private System.Windows.Forms.RadioButton radio_invoice;
        private System.Windows.Forms.RadioButton radio_PrintInvoice;
        private System.Windows.Forms.TextBox txtinvoiceno;
        private System.Windows.Forms.Label lblinvoiceno;
        private System.Windows.Forms.Button btnexit;
    }
}