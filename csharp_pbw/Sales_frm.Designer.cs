﻿namespace csharp_pbw
{
    partial class Sales_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtshippingaddress = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dropplaceofsupply = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_igst_perc = new System.Windows.Forms.Label();
            this.lbl_sgst_perc = new System.Windows.Forms.Label();
            this.lbl_cgst_perc = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtigst = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtsgst = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcgst = new System.Windows.Forms.TextBox();
            this.txtpayment = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbank = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcashdis_perc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtorderno = new System.Windows.Forms.TextBox();
            this.btncancel = new System.Windows.Forms.Button();
            this.lblinvoice = new System.Windows.Forms.Label();
            this.txtinvoiceno = new System.Windows.Forms.TextBox();
            this.cmdtransaction = new System.Windows.Forms.ComboBox();
            this.dataitem = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtlrno = new System.Windows.Forms.TextBox();
            this.txttransport = new System.Windows.Forms.TextBox();
            this.lbllr = new System.Windows.Forms.Label();
            this.lbltrans = new System.Windows.Forms.Label();
            this.lblgrandtot = new System.Windows.Forms.Label();
            this.lbldiscount = new System.Windows.Forms.Label();
            this.lbltot = new System.Windows.Forms.Label();
            this.txtgendis_perc = new System.Windows.Forms.TextBox();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.txtgeneraldiscount = new System.Windows.Forms.TextBox();
            this.txtcashdiscount = new System.Windows.Forms.TextBox();
            this.txtgrandtot = new System.Windows.Forms.TextBox();
            this.btnreset = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtamount = new System.Windows.Forms.TextBox();
            this.txtrate = new System.Windows.Forms.TextBox();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.lamount = new System.Windows.Forms.Label();
            this.lrate = new System.Windows.Forms.Label();
            this.lqty = new System.Windows.Forms.Label();
            this.cmditemname = new System.Windows.Forms.ComboBox();
            this.lbliteamname = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.cmdpartyname = new System.Windows.Forms.ComboBox();
            this.lblparty = new System.Windows.Forms.Label();
            this.btnexit = new System.Windows.Forms.Button();
            this.txtdate = new System.Windows.Forms.DateTimePicker();
            this.cmdinvoicetype = new System.Windows.Forms.ComboBox();
            this.lbldate = new System.Windows.Forms.Label();
            this.lbltransactiontype = new System.Windows.Forms.Label();
            this.lblinvoicetype = new System.Windows.Forms.Label();
            this.btninvoicelist = new System.Windows.Forms.Button();
            this.btndeleteinvoice = new System.Windows.Forms.Button();
            this.errorpartyname = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorpartyname)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.Controls.Add(this.txtshippingaddress);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.dropplaceofsupply);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lbl_igst_perc);
            this.panel1.Controls.Add(this.lbl_sgst_perc);
            this.panel1.Controls.Add(this.lbl_cgst_perc);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtigst);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtsgst);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtcgst);
            this.panel1.Controls.Add(this.txtpayment);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtbank);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtcashdis_perc);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtorderno);
            this.panel1.Controls.Add(this.btncancel);
            this.panel1.Controls.Add(this.lblinvoice);
            this.panel1.Controls.Add(this.txtinvoiceno);
            this.panel1.Controls.Add(this.cmdtransaction);
            this.panel1.Controls.Add(this.dataitem);
            this.panel1.Controls.Add(this.txtlrno);
            this.panel1.Controls.Add(this.txttransport);
            this.panel1.Controls.Add(this.lbllr);
            this.panel1.Controls.Add(this.lbltrans);
            this.panel1.Controls.Add(this.lblgrandtot);
            this.panel1.Controls.Add(this.lbldiscount);
            this.panel1.Controls.Add(this.lbltot);
            this.panel1.Controls.Add(this.txtgendis_perc);
            this.panel1.Controls.Add(this.txttotal);
            this.panel1.Controls.Add(this.txtgeneraldiscount);
            this.panel1.Controls.Add(this.txtcashdiscount);
            this.panel1.Controls.Add(this.txtgrandtot);
            this.panel1.Controls.Add(this.btnreset);
            this.panel1.Controls.Add(this.btndelete);
            this.panel1.Controls.Add(this.btnedit);
            this.panel1.Controls.Add(this.btnadd);
            this.panel1.Controls.Add(this.txtamount);
            this.panel1.Controls.Add(this.txtrate);
            this.panel1.Controls.Add(this.txtqty);
            this.panel1.Controls.Add(this.lamount);
            this.panel1.Controls.Add(this.lrate);
            this.panel1.Controls.Add(this.lqty);
            this.panel1.Controls.Add(this.cmditemname);
            this.panel1.Controls.Add(this.lbliteamname);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Controls.Add(this.cmdpartyname);
            this.panel1.Controls.Add(this.lblparty);
            this.panel1.Controls.Add(this.btnexit);
            this.panel1.Controls.Add(this.txtdate);
            this.panel1.Controls.Add(this.cmdinvoicetype);
            this.panel1.Controls.Add(this.lbldate);
            this.panel1.Controls.Add(this.lbltransactiontype);
            this.panel1.Controls.Add(this.lblinvoicetype);
            this.panel1.Controls.Add(this.btninvoicelist);
            this.panel1.Controls.Add(this.btndeleteinvoice);
            this.panel1.Location = new System.Drawing.Point(11, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1154, 642);
            this.panel1.TabIndex = 47;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtshippingaddress
            // 
            this.txtshippingaddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtshippingaddress.Location = new System.Drawing.Point(104, 92);
            this.txtshippingaddress.Name = "txtshippingaddress";
            this.txtshippingaddress.Size = new System.Drawing.Size(560, 53);
            this.txtshippingaddress.TabIndex = 84;
            this.txtshippingaddress.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 13);
            this.label9.TabIndex = 83;
            this.label9.Text = "Shipping Address : ";
            // 
            // dropplaceofsupply
            // 
            this.dropplaceofsupply.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.dropplaceofsupply.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.dropplaceofsupply.DisplayMember = "Name";
            this.dropplaceofsupply.FormattingEnabled = true;
            this.dropplaceofsupply.Location = new System.Drawing.Point(446, 59);
            this.dropplaceofsupply.Margin = new System.Windows.Forms.Padding(2);
            this.dropplaceofsupply.Name = "dropplaceofsupply";
            this.dropplaceofsupply.Size = new System.Drawing.Size(218, 21);
            this.dropplaceofsupply.TabIndex = 82;
            this.dropplaceofsupply.ValueMember = "PartyId";
            this.dropplaceofsupply.SelectedIndexChanged += new System.EventHandler(this.dropplaceofsupply_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(355, 63);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 81;
            this.label8.Text = "Place of Supply :";
            // 
            // lbl_igst_perc
            // 
            this.lbl_igst_perc.AutoSize = true;
            this.lbl_igst_perc.Location = new System.Drawing.Point(934, 569);
            this.lbl_igst_perc.Name = "lbl_igst_perc";
            this.lbl_igst_perc.Size = new System.Drawing.Size(0, 13);
            this.lbl_igst_perc.TabIndex = 80;
            // 
            // lbl_sgst_perc
            // 
            this.lbl_sgst_perc.AutoSize = true;
            this.lbl_sgst_perc.Location = new System.Drawing.Point(857, 568);
            this.lbl_sgst_perc.Name = "lbl_sgst_perc";
            this.lbl_sgst_perc.Size = new System.Drawing.Size(0, 13);
            this.lbl_sgst_perc.TabIndex = 79;
            // 
            // lbl_cgst_perc
            // 
            this.lbl_cgst_perc.AutoSize = true;
            this.lbl_cgst_perc.Location = new System.Drawing.Point(771, 569);
            this.lbl_cgst_perc.Name = "lbl_cgst_perc";
            this.lbl_cgst_perc.Size = new System.Drawing.Size(0, 13);
            this.lbl_cgst_perc.TabIndex = 78;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(902, 569);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 76;
            this.label7.Text = "IGST";
            // 
            // txtigst
            // 
            this.txtigst.Enabled = false;
            this.txtigst.Location = new System.Drawing.Point(898, 584);
            this.txtigst.Margin = new System.Windows.Forms.Padding(2);
            this.txtigst.Name = "txtigst";
            this.txtigst.ReadOnly = true;
            this.txtigst.Size = new System.Drawing.Size(91, 20);
            this.txtigst.TabIndex = 75;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(821, 568);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 74;
            this.label6.Text = "SGST";
            // 
            // txtsgst
            // 
            this.txtsgst.Enabled = false;
            this.txtsgst.Location = new System.Drawing.Point(817, 584);
            this.txtsgst.Margin = new System.Windows.Forms.Padding(2);
            this.txtsgst.Name = "txtsgst";
            this.txtsgst.ReadOnly = true;
            this.txtsgst.Size = new System.Drawing.Size(79, 20);
            this.txtsgst.TabIndex = 73;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(733, 569);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 72;
            this.label5.Text = "CGST";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtcgst
            // 
            this.txtcgst.Enabled = false;
            this.txtcgst.Location = new System.Drawing.Point(732, 584);
            this.txtcgst.Margin = new System.Windows.Forms.Padding(2);
            this.txtcgst.Name = "txtcgst";
            this.txtcgst.ReadOnly = true;
            this.txtcgst.Size = new System.Drawing.Size(83, 20);
            this.txtcgst.TabIndex = 71;
            // 
            // txtpayment
            // 
            this.txtpayment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtpayment.Location = new System.Drawing.Point(128, 533);
            this.txtpayment.Margin = new System.Windows.Forms.Padding(2);
            this.txtpayment.Name = "txtpayment";
            this.txtpayment.Size = new System.Drawing.Size(300, 20);
            this.txtpayment.TabIndex = 69;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 536);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 70;
            this.label4.Text = "Payment :";
            // 
            // txtbank
            // 
            this.txtbank.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtbank.Location = new System.Drawing.Point(128, 512);
            this.txtbank.Margin = new System.Windows.Forms.Padding(2);
            this.txtbank.Name = "txtbank";
            this.txtbank.Size = new System.Drawing.Size(300, 20);
            this.txtbank.TabIndex = 67;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 515);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 68;
            this.label3.Text = "Bank :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(668, 549);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 66;
            this.label2.Text = "Cash Discount :";
            // 
            // txtcashdis_perc
            // 
            this.txtcashdis_perc.Location = new System.Drawing.Point(753, 545);
            this.txtcashdis_perc.Margin = new System.Windows.Forms.Padding(2);
            this.txtcashdis_perc.Name = "txtcashdis_perc";
            this.txtcashdis_perc.Size = new System.Drawing.Size(56, 20);
            this.txtcashdis_perc.TabIndex = 65;
            this.txtcashdis_perc.TextChanged += new System.EventHandler(this.txtcashdis_perc_TextChanged);
            this.txtcashdis_perc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcashdis_perc_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(602, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 64;
            this.label1.Text = "Order No.";
            // 
            // txtorderno
            // 
            this.txtorderno.Location = new System.Drawing.Point(658, 28);
            this.txtorderno.Margin = new System.Windows.Forms.Padding(2);
            this.txtorderno.Name = "txtorderno";
            this.txtorderno.Size = new System.Drawing.Size(109, 20);
            this.txtorderno.TabIndex = 63;
            this.txtorderno.TextChanged += new System.EventHandler(this.txtorderno_TextChanged);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(994, 533);
            this.btncancel.Margin = new System.Windows.Forms.Padding(2);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(122, 32);
            this.btncancel.TabIndex = 22;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // lblinvoice
            // 
            this.lblinvoice.AutoSize = true;
            this.lblinvoice.Location = new System.Drawing.Point(783, 29);
            this.lblinvoice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblinvoice.Name = "lblinvoice";
            this.lblinvoice.Size = new System.Drawing.Size(59, 13);
            this.lblinvoice.TabIndex = 62;
            this.lblinvoice.Text = "Invoice No";
            // 
            // txtinvoiceno
            // 
            this.txtinvoiceno.Enabled = false;
            this.txtinvoiceno.Location = new System.Drawing.Point(845, 25);
            this.txtinvoiceno.Margin = new System.Windows.Forms.Padding(2);
            this.txtinvoiceno.Name = "txtinvoiceno";
            this.txtinvoiceno.Size = new System.Drawing.Size(144, 20);
            this.txtinvoiceno.TabIndex = 26;
            this.txtinvoiceno.TextChanged += new System.EventHandler(this.txtinvoiceno_TextChanged);
            // 
            // cmdtransaction
            // 
            this.cmdtransaction.FormattingEnabled = true;
            this.cmdtransaction.Items.AddRange(new object[] {
            "CASH",
            "DEBIT"});
            this.cmdtransaction.Location = new System.Drawing.Point(448, 28);
            this.cmdtransaction.Name = "cmdtransaction";
            this.cmdtransaction.Size = new System.Drawing.Size(140, 21);
            this.cmdtransaction.TabIndex = 1;
            this.cmdtransaction.SelectedIndexChanged += new System.EventHandler(this.cmdtransaction_SelectedIndexChanged);
            // 
            // dataitem
            // 
            this.dataitem.AllowUserToAddRows = false;
            this.dataitem.AllowUserToDeleteRows = false;
            this.dataitem.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataitem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataitem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.SalesItemId,
            this.ItemId,
            this.ItemName,
            this.Qty,
            this.Rate,
            this.Amount});
            this.dataitem.Location = new System.Drawing.Point(33, 194);
            this.dataitem.Name = "dataitem";
            this.dataitem.ReadOnly = true;
            this.dataitem.RowHeadersVisible = false;
            this.dataitem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataitem.Size = new System.Drawing.Size(957, 301);
            this.dataitem.TabIndex = 55;
            this.dataitem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataitem_CellClick);
            this.dataitem.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataitem_DataBindingComplete);
            // 
            // No
            // 
            this.No.DataPropertyName = "No";
            this.No.HeaderText = "No";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.No.Width = 55;
            // 
            // SalesItemId
            // 
            this.SalesItemId.DataPropertyName = "SalesItemId";
            this.SalesItemId.HeaderText = "SalesItemId";
            this.SalesItemId.Name = "SalesItemId";
            this.SalesItemId.ReadOnly = true;
            this.SalesItemId.Visible = false;
            // 
            // ItemId
            // 
            this.ItemId.DataPropertyName = "ItemId";
            this.ItemId.HeaderText = "ItemId";
            this.ItemId.Name = "ItemId";
            this.ItemId.ReadOnly = true;
            this.ItemId.Visible = false;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "ItemName";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ItemName.Width = 400;
            // 
            // Qty
            // 
            this.Qty.DataPropertyName = "Qty";
            this.Qty.HeaderText = "Qty";
            this.Qty.Name = "Qty";
            this.Qty.ReadOnly = true;
            this.Qty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Rate
            // 
            this.Rate.DataPropertyName = "Rate";
            this.Rate.HeaderText = "Rate";
            this.Rate.Name = "Rate";
            this.Rate.ReadOnly = true;
            this.Rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtlrno
            // 
            this.txtlrno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtlrno.Location = new System.Drawing.Point(130, 577);
            this.txtlrno.Margin = new System.Windows.Forms.Padding(2);
            this.txtlrno.Name = "txtlrno";
            this.txtlrno.Size = new System.Drawing.Size(160, 20);
            this.txtlrno.TabIndex = 17;
            // 
            // txttransport
            // 
            this.txttransport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txttransport.Location = new System.Drawing.Point(129, 555);
            this.txttransport.Margin = new System.Windows.Forms.Padding(2);
            this.txttransport.Name = "txttransport";
            this.txttransport.Size = new System.Drawing.Size(300, 20);
            this.txttransport.TabIndex = 16;
            // 
            // lbllr
            // 
            this.lbllr.AutoSize = true;
            this.lbllr.Location = new System.Drawing.Point(43, 582);
            this.lbllr.Name = "lbllr";
            this.lbllr.Size = new System.Drawing.Size(53, 13);
            this.lbllr.TabIndex = 52;
            this.lbllr.Text = "L.R. No. :";
            // 
            // lbltrans
            // 
            this.lbltrans.AutoSize = true;
            this.lbltrans.Location = new System.Drawing.Point(38, 559);
            this.lbltrans.Name = "lbltrans";
            this.lbltrans.Size = new System.Drawing.Size(61, 13);
            this.lbltrans.TabIndex = 51;
            this.lbltrans.Text = "Transport : ";
            // 
            // lblgrandtot
            // 
            this.lblgrandtot.AutoSize = true;
            this.lblgrandtot.Location = new System.Drawing.Point(705, 610);
            this.lblgrandtot.Name = "lblgrandtot";
            this.lblgrandtot.Size = new System.Drawing.Size(69, 13);
            this.lblgrandtot.TabIndex = 50;
            this.lblgrandtot.Text = "Grand Total :";
            // 
            // lbldiscount
            // 
            this.lbldiscount.AutoSize = true;
            this.lbldiscount.Location = new System.Drawing.Point(655, 526);
            this.lbldiscount.Name = "lbldiscount";
            this.lbldiscount.Size = new System.Drawing.Size(95, 13);
            this.lbldiscount.TabIndex = 48;
            this.lbldiscount.Text = "General Discount :";
            // 
            // lbltot
            // 
            this.lbltot.AutoSize = true;
            this.lbltot.Location = new System.Drawing.Point(772, 504);
            this.lbltot.Name = "lbltot";
            this.lbltot.Size = new System.Drawing.Size(37, 13);
            this.lbltot.TabIndex = 47;
            this.lbltot.Text = "Total :";
            // 
            // txtgendis_perc
            // 
            this.txtgendis_perc.Location = new System.Drawing.Point(753, 522);
            this.txtgendis_perc.Margin = new System.Windows.Forms.Padding(2);
            this.txtgendis_perc.Name = "txtgendis_perc";
            this.txtgendis_perc.Size = new System.Drawing.Size(56, 20);
            this.txtgendis_perc.TabIndex = 18;
            this.txtgendis_perc.TextChanged += new System.EventHandler(this.txtgendis_perc_TextChanged);
            this.txtgendis_perc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgendis_perc_KeyPress);
            // 
            // txttotal
            // 
            this.txttotal.Enabled = false;
            this.txttotal.Location = new System.Drawing.Point(812, 500);
            this.txttotal.Margin = new System.Windows.Forms.Padding(2);
            this.txttotal.Name = "txttotal";
            this.txttotal.ReadOnly = true;
            this.txttotal.Size = new System.Drawing.Size(178, 20);
            this.txttotal.TabIndex = 45;
            this.txttotal.TextChanged += new System.EventHandler(this.txttotal_TextChanged);
            // 
            // txtgeneraldiscount
            // 
            this.txtgeneraldiscount.Location = new System.Drawing.Point(812, 522);
            this.txtgeneraldiscount.Margin = new System.Windows.Forms.Padding(2);
            this.txtgeneraldiscount.Name = "txtgeneraldiscount";
            this.txtgeneraldiscount.Size = new System.Drawing.Size(178, 20);
            this.txtgeneraldiscount.TabIndex = 19;
            this.txtgeneraldiscount.TextChanged += new System.EventHandler(this.txtgeneraldiscount_TextChanged);
            this.txtgeneraldiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgeneraldiscount_KeyPress);
            this.txtgeneraldiscount.Leave += new System.EventHandler(this.txtgeneraldiscount_Leave);
            // 
            // txtcashdiscount
            // 
            this.txtcashdiscount.Location = new System.Drawing.Point(812, 545);
            this.txtcashdiscount.Margin = new System.Windows.Forms.Padding(2);
            this.txtcashdiscount.Name = "txtcashdiscount";
            this.txtcashdiscount.Size = new System.Drawing.Size(178, 20);
            this.txtcashdiscount.TabIndex = 20;
            this.txtcashdiscount.TextChanged += new System.EventHandler(this.txtcashdiscount_TextChanged);
            this.txtcashdiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcashdiscount_KeyPress);
            this.txtcashdiscount.Leave += new System.EventHandler(this.txtcashdiscount_Leave);
            // 
            // txtgrandtot
            // 
            this.txtgrandtot.Enabled = false;
            this.txtgrandtot.Location = new System.Drawing.Point(811, 606);
            this.txtgrandtot.Margin = new System.Windows.Forms.Padding(2);
            this.txtgrandtot.Name = "txtgrandtot";
            this.txtgrandtot.ReadOnly = true;
            this.txtgrandtot.Size = new System.Drawing.Size(178, 20);
            this.txtgrandtot.TabIndex = 42;
            // 
            // btnreset
            // 
            this.btnreset.Location = new System.Drawing.Point(995, 272);
            this.btnreset.Margin = new System.Windows.Forms.Padding(2);
            this.btnreset.Name = "btnreset";
            this.btnreset.Size = new System.Drawing.Size(122, 32);
            this.btnreset.TabIndex = 15;
            this.btnreset.Text = "RESET";
            this.btnreset.UseVisualStyleBackColor = true;
            this.btnreset.Click += new System.EventHandler(this.btnreset_Click);
            // 
            // btndelete
            // 
            this.btndelete.Location = new System.Drawing.Point(995, 236);
            this.btndelete.Margin = new System.Windows.Forms.Padding(2);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(122, 32);
            this.btndelete.TabIndex = 14;
            this.btndelete.Text = "DELETE";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnedit
            // 
            this.btnedit.Location = new System.Drawing.Point(995, 200);
            this.btnedit.Margin = new System.Windows.Forms.Padding(2);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(122, 32);
            this.btnedit.TabIndex = 13;
            this.btnedit.Text = "UPDATE";
            this.btnedit.UseVisualStyleBackColor = true;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.Location = new System.Drawing.Point(995, 164);
            this.btnadd.Margin = new System.Windows.Forms.Padding(2);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(122, 32);
            this.btnadd.TabIndex = 12;
            this.btnadd.Text = "ADD";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // txtamount
            // 
            this.txtamount.Location = new System.Drawing.Point(664, 169);
            this.txtamount.Margin = new System.Windows.Forms.Padding(2);
            this.txtamount.Name = "txtamount";
            this.txtamount.Size = new System.Drawing.Size(189, 20);
            this.txtamount.TabIndex = 11;
            this.txtamount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtamount_KeyPress);
            // 
            // txtrate
            // 
            this.txtrate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtrate.Location = new System.Drawing.Point(534, 169);
            this.txtrate.Margin = new System.Windows.Forms.Padding(2);
            this.txtrate.Name = "txtrate";
            this.txtrate.Size = new System.Drawing.Size(119, 20);
            this.txtrate.TabIndex = 7;
            this.txtrate.TextChanged += new System.EventHandler(this.txtrate_TextChanged);
            this.txtrate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrate_KeyPress);
            // 
            // txtqty
            // 
            this.txtqty.Location = new System.Drawing.Point(458, 169);
            this.txtqty.Margin = new System.Windows.Forms.Padding(2);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(56, 20);
            this.txtqty.TabIndex = 6;
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            this.txtqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqty_KeyPress);
            // 
            // lamount
            // 
            this.lamount.AutoSize = true;
            this.lamount.Location = new System.Drawing.Point(661, 153);
            this.lamount.Name = "lamount";
            this.lamount.Size = new System.Drawing.Size(43, 13);
            this.lamount.TabIndex = 27;
            this.lamount.Text = "Amount";
            // 
            // lrate
            // 
            this.lrate.AutoSize = true;
            this.lrate.Location = new System.Drawing.Point(531, 154);
            this.lrate.Name = "lrate";
            this.lrate.Size = new System.Drawing.Size(30, 13);
            this.lrate.TabIndex = 23;
            this.lrate.Text = "Rate";
            // 
            // lqty
            // 
            this.lqty.AutoSize = true;
            this.lqty.Location = new System.Drawing.Point(455, 153);
            this.lqty.Name = "lqty";
            this.lqty.Size = new System.Drawing.Size(23, 13);
            this.lqty.TabIndex = 22;
            this.lqty.Text = "Qty";
            // 
            // cmditemname
            // 
            this.cmditemname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmditemname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmditemname.DisplayMember = "ItemName";
            this.cmditemname.FormattingEnabled = true;
            this.cmditemname.Location = new System.Drawing.Point(33, 169);
            this.cmditemname.Margin = new System.Windows.Forms.Padding(2);
            this.cmditemname.Name = "cmditemname";
            this.cmditemname.Size = new System.Drawing.Size(395, 21);
            this.cmditemname.TabIndex = 4;
            this.cmditemname.ValueMember = "ItemId";
            // 
            // lbliteamname
            // 
            this.lbliteamname.AutoSize = true;
            this.lbliteamname.Location = new System.Drawing.Point(30, 153);
            this.lbliteamname.Name = "lbliteamname";
            this.lbliteamname.Size = new System.Drawing.Size(58, 13);
            this.lbliteamname.TabIndex = 14;
            this.lbliteamname.Text = "Item Name";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(994, 500);
            this.btnok.Margin = new System.Windows.Forms.Padding(2);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(122, 32);
            this.btnok.TabIndex = 21;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // cmdpartyname
            // 
            this.cmdpartyname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmdpartyname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmdpartyname.DisplayMember = "Name";
            this.cmdpartyname.FormattingEnabled = true;
            this.cmdpartyname.Location = new System.Drawing.Point(104, 59);
            this.cmdpartyname.Margin = new System.Windows.Forms.Padding(2);
            this.cmdpartyname.Name = "cmdpartyname";
            this.cmdpartyname.Size = new System.Drawing.Size(236, 21);
            this.cmdpartyname.TabIndex = 3;
            this.cmdpartyname.ValueMember = "PartyId";
            // 
            // lblparty
            // 
            this.lblparty.AutoSize = true;
            this.lblparty.Location = new System.Drawing.Point(30, 63);
            this.lblparty.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblparty.Name = "lblparty";
            this.lblparty.Size = new System.Drawing.Size(71, 13);
            this.lblparty.TabIndex = 10;
            this.lblparty.Text = "Party Name : ";
            // 
            // btnexit
            // 
            this.btnexit.CausesValidation = false;
            this.btnexit.Location = new System.Drawing.Point(995, 569);
            this.btnexit.Margin = new System.Windows.Forms.Padding(2);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(122, 32);
            this.btnexit.TabIndex = 22;
            this.btnexit.Text = "EXIT";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // txtdate
            // 
            this.txtdate.CustomFormat = "d/m/yyyy";
            this.txtdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtdate.Location = new System.Drawing.Point(718, 59);
            this.txtdate.Margin = new System.Windows.Forms.Padding(2);
            this.txtdate.Name = "txtdate";
            this.txtdate.Size = new System.Drawing.Size(109, 20);
            this.txtdate.TabIndex = 2;
            // 
            // cmdinvoicetype
            // 
            this.cmdinvoicetype.FormattingEnabled = true;
            this.cmdinvoicetype.Items.AddRange(new object[] {
            "TAX",
            "RETAIL"});
            this.cmdinvoicetype.Location = new System.Drawing.Point(104, 27);
            this.cmdinvoicetype.Margin = new System.Windows.Forms.Padding(2);
            this.cmdinvoicetype.Name = "cmdinvoicetype";
            this.cmdinvoicetype.Size = new System.Drawing.Size(165, 21);
            this.cmdinvoicetype.TabIndex = 0;
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Location = new System.Drawing.Point(677, 63);
            this.lbldate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(39, 13);
            this.lbldate.TabIndex = 5;
            this.lbldate.Text = "Date : ";
            // 
            // lbltransactiontype
            // 
            this.lbltransactiontype.AutoSize = true;
            this.lbltransactiontype.Location = new System.Drawing.Point(343, 32);
            this.lbltransactiontype.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbltransactiontype.Name = "lbltransactiontype";
            this.lbltransactiontype.Size = new System.Drawing.Size(102, 13);
            this.lbltransactiontype.TabIndex = 4;
            this.lbltransactiontype.Text = "Transaction Type  : ";
            // 
            // lblinvoicetype
            // 
            this.lblinvoicetype.AutoSize = true;
            this.lblinvoicetype.Location = new System.Drawing.Point(23, 31);
            this.lblinvoicetype.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblinvoicetype.Name = "lblinvoicetype";
            this.lblinvoicetype.Size = new System.Drawing.Size(78, 13);
            this.lblinvoicetype.TabIndex = 3;
            this.lblinvoicetype.Text = "Invoice Type : ";
            // 
            // btninvoicelist
            // 
            this.btninvoicelist.Location = new System.Drawing.Point(994, 24);
            this.btninvoicelist.Name = "btninvoicelist";
            this.btninvoicelist.Size = new System.Drawing.Size(149, 32);
            this.btninvoicelist.TabIndex = 23;
            this.btninvoicelist.Text = "VIEW INVOICE";
            this.btninvoicelist.UseVisualStyleBackColor = true;
            this.btninvoicelist.Click += new System.EventHandler(this.btninvoicelist_Click);
            // 
            // btndeleteinvoice
            // 
            this.btndeleteinvoice.Location = new System.Drawing.Point(995, 61);
            this.btndeleteinvoice.Margin = new System.Windows.Forms.Padding(2);
            this.btndeleteinvoice.Name = "btndeleteinvoice";
            this.btndeleteinvoice.Size = new System.Drawing.Size(149, 33);
            this.btndeleteinvoice.TabIndex = 24;
            this.btndeleteinvoice.Text = "DELETE INVOICE";
            this.btndeleteinvoice.UseVisualStyleBackColor = true;
            this.btndeleteinvoice.Click += new System.EventHandler(this.btndeleteinvoice_Click);
            // 
            // errorpartyname
            // 
            this.errorpartyname.ContainerControl = this;
            // 
            // Sales_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1179, 668);
            this.Controls.Add(this.panel1);
            this.Name = "Sales_frm";
            this.Text = "Invoice";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorpartyname)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label lblinvoice;
        private System.Windows.Forms.TextBox txtinvoiceno;
        private System.Windows.Forms.ComboBox cmdtransaction;
        private System.Windows.Forms.DataGridView dataitem;
        private System.Windows.Forms.TextBox txtlrno;
        private System.Windows.Forms.TextBox txttransport;
        private System.Windows.Forms.Label lbllr;
        private System.Windows.Forms.Label lbltrans;
        private System.Windows.Forms.Label lblgrandtot;
        private System.Windows.Forms.Label lbldiscount;
        private System.Windows.Forms.Label lbltot;
        private System.Windows.Forms.TextBox txtgendis_perc;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.TextBox txtgeneraldiscount;
        private System.Windows.Forms.TextBox txtcashdiscount;
        private System.Windows.Forms.TextBox txtgrandtot;
        private System.Windows.Forms.Button btnreset;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.TextBox txtamount;
        private System.Windows.Forms.TextBox txtrate;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label lamount;
        private System.Windows.Forms.Label lrate;
        private System.Windows.Forms.Label lqty;
        private System.Windows.Forms.ComboBox cmditemname;
        private System.Windows.Forms.Label lbliteamname;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.ComboBox cmdpartyname;
        private System.Windows.Forms.Label lblparty;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.DateTimePicker txtdate;
        private System.Windows.Forms.ComboBox cmdinvoicetype;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Label lbltransactiontype;
        private System.Windows.Forms.Label lblinvoicetype;
        private System.Windows.Forms.Button btninvoicelist;
        private System.Windows.Forms.Button btndeleteinvoice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtorderno;
        private System.Windows.Forms.TextBox txtbank;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcashdis_perc;
        private System.Windows.Forms.TextBox txtpayment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ErrorProvider errorpartyname;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtigst;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtsgst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcgst;
        private System.Windows.Forms.Label lbl_igst_perc;
        private System.Windows.Forms.Label lbl_sgst_perc;
        private System.Windows.Forms.Label lbl_cgst_perc;
        private System.Windows.Forms.ComboBox dropplaceofsupply;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox txtshippingaddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
    }
}