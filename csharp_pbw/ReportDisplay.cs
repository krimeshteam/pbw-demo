﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;

namespace csharp_pbw
{
    public partial class ReportDisplay : Form
    {
        public ReportDisplay()
        {
            InitializeComponent();
            if (!string.IsNullOrEmpty(Common.ReportType))
            {
                bind();
            }
        }

        public void bind()
        {
            if (string.IsNullOrEmpty(Common.ReportType))
            {
                this.Hide();
            }
            else
            {
                string reportPath_pg = null;

                ReportDocument Rpt_pg = new ReportDocument();

                if (Common.ReportType == "Item")
                {
                    string query = "SELECT * FROM ItemMaster";
                    DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            reportPath_pg = Application.StartupPath + @"\Report\ItemReport.rpt";
                            Rpt_pg.Load(reportPath_pg);
                            Rpt_pg.SetDataSource(ds.Tables[0]);
                            crystalReportViewer1.ReportSource = Rpt_pg;
                        }
                        else
                        {
                            this.Hide();
                        }
                    }
                    else
                    {
                        this.Hide();
                    }
                }
                else if (Common.ReportType == "Party")
                {
                    string query = "SELECT * FROM PartyMaster";
                    DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            reportPath_pg = Application.StartupPath + @"\Report\PartyReport.rpt";
                            Rpt_pg.Load(reportPath_pg);
                            Rpt_pg.SetDataSource(ds.Tables[0]);
                            crystalReportViewer1.ReportSource = Rpt_pg;
                        }
                        else
                        {
                            this.Hide();
                        }
                    }
                    else
                    {
                        this.Hide();
                    }
                }
                else if (Common.ReportType == "Order")
                {
                    string query = "select OrderMaster.OrderId,PartyMaster.PartyName,OrderMaster.OrderNO,OrderMaster.OrderDate,OrderItemMaster.Qty,OrderItemMaster.Rate,ItemMaster.ItemName from OrderMaster " +
                            " inner join OrderItemMaster on OrderMaster.OrderId = OrderItemMaster.OrderId" +
                            " inner join ItemMaster on OrderItemMaster.ItemId = ItemMaster.ItemId" +
                            " inner join PartyMaster on OrderMaster.PartyId = PartyMaster.PartyId";
                    DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            reportPath_pg = Application.StartupPath + @"\Report\OrderReport.rpt";
                            Rpt_pg.Load(reportPath_pg);
                            Rpt_pg.SetDataSource(ds.Tables[0]);
                            crystalReportViewer1.ReportSource = Rpt_pg;
                        }
                        else
                        {
                            this.Hide();
                        }
                    }
                    else
                    {
                        this.Hide();
                    }
                }
                else if (Common.ReportType == "InvoiceList")
                {
                    string query = "select distinct SalesMaster.SalesNo as InvoiceNo,PartyMaster.PartyName as Name,SalesMaster.SalesDate as Date,SalesMaster.InvoiceType,SalesMaster.GrandTotal from SalesMaster inner join PartyMaster on SalesMaster.PartyId = PartyMaster.PartyId where SalesMaster.IsDelete=0";
                    DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            reportPath_pg = Application.StartupPath + @"\Report\InvoiceList.rpt";
                            Rpt_pg.Load(reportPath_pg);
                            Rpt_pg.SetDataSource(ds.Tables[0]);
                            crystalReportViewer1.ReportSource = Rpt_pg;
                        }
                        else
                        {
                            this.Hide();
                        }
                    }
                    else
                    {
                        this.Hide();
                    }
                }
                else if (Common.ReportType == "PrintInvoice")
                {
                    string query = "select SalesMaster.*,SalesItemMaster.Qty,SalesItemMaster.Rate,StateMaster.State,StateMaster.StateCode,PartyMaster.PartyName,PartyMaster.Address as PartyAddress,PartyMaster.Phone as PartyPhone,PartyMaster.Email as PartyEmail,PartyMaster.Fax as PartyFax,PartyMaster.GstIn as PartyGstIn,ItemMaster.ItemName" +
                            " from SalesMaster inner join SalesItemMaster on SalesMaster.SalesId = SalesItemMaster.SalesId" +
                            " inner join PartyMaster on SalesMaster.PartyId = PartyMaster.PartyId" +
                            " inner join StateMaster on SalesMaster.PlaceofSupply = StateMaster.StateId" +
                            " inner join ItemMaster on SalesItemMaster.ItemId = ItemMaster.ItemId where SalesMaster.SalesId=" + Common.InvoiceNo;
                    DataSet ds = SqlHelper.ExecuteDataset(ConnString.GetConnString(), CommandType.Text, query);
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            reportPath_pg = Application.StartupPath + @"\Report\Invoice.rpt";
                            Rpt_pg.Load(reportPath_pg);
                            Rpt_pg.SetDataSource(ds.Tables[0]);
                            crystalReportViewer1.ReportSource = Rpt_pg;
                        }
                        else
                        {
                            this.Hide();
                        }
                    }
                    else
                    {
                        this.Hide();
                    }
                }

                ///-------------------set_logon function in class set logon-----------------------------------------------------------//
                Rpt_pg.DataSourceConnections[0].IntegratedSecurity = true;
                // and/or 
                //Rpt_pg.DataSourceConnections[0].SetConnection(@"(LocalDB)\MSSQLLocalDB", "PatelDatabase", true);
                crystalReportViewer1.ReportSource = Rpt_pg;


                //if (Rpt_pg != null)
                //{
                //    Rpt_pg.Close();
                //    Rpt_pg.Dispose();
                //}

            }
        }

    }
}
