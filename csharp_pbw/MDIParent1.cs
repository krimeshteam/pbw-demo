﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp_pbw
{
    public partial class MDIParent1 : Form
    {
        
        public MDIParent1()
        {
            InitializeComponent();
          }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure want to close program?", "Confirmation", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void salesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sales_frm obj = new Sales_frm();
            obj.MdiParent = this;
            obj.WindowState = FormWindowState.Maximized;
            obj.Text = "Invoice";
            obj.Show();
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportForm obj = new ReportForm();
            obj.MdiParent = this;
            obj.Text = "Report";
            obj.WindowState = FormWindowState.Maximized;
            obj.Show();
        }

        private void MDIParent1_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
